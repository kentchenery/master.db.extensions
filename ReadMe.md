# master.db.extensions
This project is a collection of procedures that can be added to the master database in SQL Server to assist in database administration.

It is defined as an SSDT project to aid development and deployment.  Although it is possible to simply cherry pick the procedures you want by simply running the scripts for those procedures.

## Goals
The aim is simple.  Make database administration a little easier. These procedures improve on existing system procedures or wrap up typical tasks (reindexing, backups, checkdb etc).

