﻿/* Procedures */
:r "..\dbo\Procedures\sp_spaceused2.sql"
:r "..\dbo\Procedures\sp_helpindex2.sql"
:r "..\dbo\Procedures\sp_backup_databases.sql"
:r "..\dbo\Procedures\sp_delete_old_files.sql"
:r "..\dbo\Procedures\sp_check_databases.sql"
:r "..\dbo\Procedures\sp_reindex.sql"
:r "..\dbo\Procedures\sp_who3.sql"
:r "..\dbo\Procedures\sp_updatestats2.sql"
:r "..\dbo\Procedures\sp_fix_orphaned_uesrs.sql"
:r "..\dbo\Views\Uptime.sql"
GO

/* Delete the old procedure if it exists */
IF EXISTS(SELECT 1 FROM sys.procedures WHERE name LIKE 'sp_delete_old_backups')
BEGIN;
    DROP PROCEDURE sp_delete_old_backups;
END;
GO