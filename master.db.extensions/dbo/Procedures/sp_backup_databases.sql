﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_backup_databases';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_backup_databases')
	EXEC ('CREATE PROC dbo.sp_backup_databases AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE [dbo].[sp_backup_databases]
	@Databases					NVARCHAR(MAX)	= '%'		/* Include all */
	,@Delimiter					NVARCHAR(10)	= ','
	,@BackupPath				NVARCHAR(1000)	= NULL		/* When null its looked up from the configuration */
	,@CompressBackups			BIT				= NULL		/* When null its looked up from the configuration */
	,@NumberOfFiles				INT				= 1
	,@BackupType				NVARCHAR(10)	= 'FULL'
	,@CopyOnly					BIT				= 0
	,@Stats						TINYINT			= 10
	,@Initialise				BIT				= 1
	,@SeparateDatabaseFolders	BIT				= 1
	,@IgnoreDatabases			NVARCHAR(MAX)	= NULL		/* Ignore none */
	,@Checksum					BIT				= 0
	,@ContinueAfterError		BIT				= 0
	,@OnlineDatabasesOnly		BIT				= 1			/* Only backup databases that are in an online state */
	,@IgnoreInStandby			BIT				= 1			/* Ignore databases that are in standby mode */
    ,@BackupSecondaryReplicas   BIT             = 0         /* When 1 secondary replicas will be backed up too. When 0 they are ignored */
	,@WhatIf					BIT				= 0			/* Similar to -WhatIf in PowerShell. Displat what would happen rather than actually doing it */
AS
/*
---
Description:    Performs a backup of all the databases supplied.  The database backup file 
				names are in the format of: DatabaseName_[FULL|DIFF|LOG]_YYYYMMDD_HHMMSS_[FileNumber].[BAK|DIFF|TRN]

Parameters:
 - Databases:				Delimited list of databases names. This parameter accepts the % wildcard and will match on that.
 - Delimiter:				The delimiter used in @Databases
 - BackupPath:				The base path for backup files.  If NULL it is set to the instance default
 - CompressBackups:			Flag to indicate if backups should be compressed.  If NULL it is set to the instance default
 - NumberOfFiles:			Number of files to backup to with a default of 1
 - BackupType:				The type of backup to take
 - CopyOnly:				Whether COPY_ONLY backups are taken
 - Stats:					Percentage at which to output information.  0 = no output
 - Initialise:				Whether the backup file is intialised or not
 - SeparateDatabaseFolders:	Flat to indicate whether databases are backed up into their own folders
 - IgnoreDatabases:			Like the Databases parameter, but an ignore list.  Also accepts wildcards.

Output:
 - Output that you get from running the BACKUP command
...
*/
BEGIN;
	SET NOCOUNT ON;

	DECLARE @CRLF NCHAR(2);
	SET @CRLF = CHAR(13) + CHAR(10);

    SELECT @BackupType = UPPER(@BackupType);

	/* Sanity check the variables */
	IF @BackupType NOT IN ('FULL', 'DIFF', 'LOG')
	BEGIN;
		RAISERROR('@BackupType must be one of: FULL, DIFF, LOG', 16, 1);
	END;

	IF ISNULL(@NumberOfFiles, 0) < 1
	BEGIN;
		RAISERROR('@NumberOfFiles is out of bounds (> 0)', 16, 1);
	END;

	IF @Stats > 100
	BEGIN;
		RAISERROR('@Stats is out of bounds (<100)', 16, 1);
	END;

	/* Validate some inputs */
	SELECT
		@Stats = ISNULL(@Stats, 0)
		,@Initialise = ISNULl(@Initialise, 0)
		,@CopyOnly = ISNULL(@CopyOnly, 0)
	;

	/* Create a numer table so we can perform tricks */
	DECLARE @NumbersTable TABLE(
		Num	INT
	);

	WITH N1(Num) AS (
		SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL 
		SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1
	), N2(Num) AS (
		SELECT a.Num
		FROM
			N1 AS a
			CROSS JOIN N1 AS b
	)
	INSERT INTO @NumbersTable
	SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) - 1	/* 0 base the list */
	FROM N2
	WHERE N2.Num < 100;	/* Only need 100 rows */

	/* Look up the default backup path if none supplied */
	IF @BackupPath IS NULL
	BEGIN;
		EXEC master.dbo.xp_instance_regread
			N'HKEY_LOCAL_MACHINE'
			,N'Software\Microsoft\MSSQLServer\MSSQLServer'
			,N'BackupDirectory'
			,@BackupPath OUTPUT
			,'no_output';
	END;

	/* Determine the folder separator */
	DECLARE @FolderSeparator	CHAR(1);

	IF LEFT(@BackupPath, 1) = '/'	/* Is it on Linux? */
	BEGIN;
		SET @FolderSeparator = '/';
	END;
	ELSE
	BEGIN;
		SET @FolderSeparator = '\';
	END;

	/* Strip trailing @FolderSeparator from @BackupPath */
	IF RIGHT(@BackupPath, 1) = @FolderSeparator
	BEGIN;
		SET @BackupPath = LEFT(@BackupPath, LEN(@BackupPath) - 1);
	END;

	/* Determine if backup compression should take place */
	IF @CompressBackups IS NULL
	BEGIN;
		SELECT @CompressBackups = CONVERT(BIT, value)
		FROM
			sys.configurations
		WHERE
			[name] = 'backup compression default'
		;
	END;

	/* Check values */
	SELECT
		@NumberOfFiles		= ISNULL(@NumberOfFiles, 1)			/* Default files to 1 if not supplied */
		,@CompressBackups	= ISNULL(@CompressBackups, 0)		/* Do not compress backups if could not be determined */

	/* Generate the list of databases to backup.  Use the FOR XML trick/hack to pivot the @Databases list */
	CREATE TABLE #DatabasesToBackup(
		DatabaseName	NVARCHAR(128) PRIMARY KEY CLUSTERED
	);

	DECLARE @DatabaseListXML	XML;
	SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@Databases, @Delimiter, '</db><db>') + '</db>' AS XML);

	WITH DatabaseList(DatabaseName) AS (
		SELECT DISTINCT
			LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
		FROM
			@DatabaseListXML.nodes('db') AS DB(N)
	)
	INSERT INTO #DatabasesToBackup(DatabaseName)
	SELECT
		LTRIM(RTRIM(d.[name]))	AS DatabaseName		/* Strip off leading and trailing spaces from database names (because they cause problems) */
	FROM
		sys.databases AS d
	WHERE EXISTS(
			SELECT 1 FROM DatabaseList WHERE d.name LIKE DatabaseName
		)
	AND	d.database_id != 2	/* Ignore tempdb */
	AND d.source_database_id IS NULL /* Ignore database snapshots */
	AND	(
		/* Ignore master if a diff backup */
			d.database_id != 1
		OR	@BackupType != 'DIFF'
	)
	AND (
		/* Ignore log backups for databases in SIMPLE recovery */
			d.recovery_model_desc != 'SIMPLE'
		OR	@BackupType != 'LOG'
	)
	AND (
		/* Online databases only */
			d.[state] = 0					/* Online */
		OR	@OnlineDatabasesOnly = 0
	)
	AND (
			d.is_in_standby = 0
		OR	@IgnoreInStandby = 1
	)
	;


    /* Remove secondary replicas if they are not being backed up */
    IF (@BackupSecondaryReplicas = 0 AND OBJECT_ID('sys.dm_hadr_database_replica_states') IS NOT NULL)
    BEGIN;
        DECLARE @SQL    NVARCHAR(MAX);
        SELECT @SQL = N'
        DELETE FROM b
        FROM
            #DatabasesToBackup AS b
            INNER JOIN sys.databases AS d
                ON b.DatabaseName = d.name
            INNER JOIN sys.dm_hadr_database_replica_states AS drs 
                ON drs.database_id = d.database_id
            INNER JOIN sys.dm_hadr_availability_replica_states AS ars 
                ON drs.replica_id = ars.replica_id
        WHERE 
	        ars.role_desc != ''PRIMARY''
        AND ars.is_local = 1
        ';

        EXEC sp_executesql @SQL;
    END;

	SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@IgnoreDatabases, @Delimiter, '</db><db>') + '</db>' AS XML);

	WITH IgnoreDatabaseList(DatabaseName) AS (
		SELECT DISTINCT
			LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
		FROM
			@DatabaseListXML.nodes('db') AS DB(N)
	)
	DELETE dtp
	FROM #DatabasesToBackup AS dtp
	WHERE 
		EXISTS(SELECT 1 FROM IgnoreDatabaseList AS idl WHERE dtp.DatabaseName LIKE idl.DatabaseName);

	/* Dictionary lookup table for file extensions based on type */
	DECLARE @BackupTypeDictionary TABLE (
		[Key]		NVARCHAR(100)
		,[Value]	NVARCHAR(100)
	);

	INSERT INTO @BackupTypeDictionary([Key], [Value])
	SELECT 'FULL', 'BAK' UNION ALL
	SELECT 'DIFF', 'DIFF' UNION ALL
	SELECT 'LOG', 'TRN'

	/* Determine the full backup path.  File name is in the format: DatabaseName_[FULL|DIFF|LOG]_YYYYMMDD_HHMMSS_[FileNumber].[BAK|DIFF|TRN] */
	DECLARE @FullBackupPath	NVARCHAR(MAX);
	SELECT 
		@FullBackupPath = @BackupPath + CASE WHEN @SeparateDatabaseFolders = 1 THEN @FolderSeparator + '##DATABASE##' ELSE '' END + @FolderSeparator + '##DATABASE##_' + @BackupType + '_##TIMESTAMP##_##FILENUMBER##.' + [Value] 
	FROM
		@BackupTypeDictionary 
	WHERE
		[Key] = @BackupType
	;

	/* Start building the backup command */
	DECLARE @BackupCommandStub	NVARCHAR(MAX);
	SELECT 
		@BackupCommandStub = 'BACKUP ' + 
		CASE
			WHEN @BackupType = 'LOG' THEN 'LOG'
			ELSE 'DATABASE'
		END + 
		' ##DATABASE##
TO
';

	/* WITH options */
	DECLARE @BackupWith	NVARCHAR(MAX);
	SELECT @BackupWith = ''

	SELECT @BackupWith = @BackupWith +
		CASE WHEN @BackupType = 'DIFF'		THEN '    ,DIFFERENTIAL' + @CRLF ELSE '' END +
		CASE WHEN @Initialise = 1			THEN '    ,INIT' + @CRLF ELSE '' END +
		CASE WHEN @Stats > 0				THEN '    ,STATS = ' + CONVERT(NVARCHAR(10), @Stats) + @CRLF ELSE '' END +
		CASE WHEN @CopyOnly = 1				THEN '    ,COPY_ONLY' + @CRLF ELSE '' END +
		CASE WHEN @CompressBackups = 1		THEN '    ,COMPRESSION' + @CRLF ELSE '' END +
		CASE WHEN @Checksum = 1				THEN '    ,CHECKSUM' + @CRLF ELSE '' END +
		CASE WHEN @ContinueAfterError = 1	THEN '    ,CONTINUE_AFTER_ERROR' + @CRLF ELSE '' END

	SELECT @BackupWith = STUFF(@BackupWith, 1, 5, 'WITH' + @CRLF + '    ')

	/* For each database */
	DECLARE @DatabaseName				NVARCHAR(128);
	DECLARE @BackupCommandStubFileList	NVARCHAR(MAX);
	DECLARE @BackupCommand				NVARCHAR(MAX);
	DECLARE @BackupDatabaseFolder		NVARCHAR(1024);
	DECLARE @StartedAt					DATETIME;

	DECLARE @BackupTimings				TABLE (
		DatabaseName			NVARCHAR(128)
		,BackupType				NVARCHAR(10)
		,StartedAt				DATETIME
		,FinishedAt				DATETIME
		,DurationSeconds		AS DATEDIFF(SECOND, StartedAt, FinishedAt)
	);

	DECLARE cDatabase CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT
		DatabaseName
	FROM
		#DatabasesToBackup
	ORDER BY
		DatabaseName	/* Ensure the order is known and consistent between executions */
	;

	OPEN cDatabase
	FETCH NEXT FROM cDatabase INTO @DatabaseName;

	WHILE @@FETCH_STATUS = 0
	BEGIN;
		/* If its a log backup make sure its able to have a log backup taken */
		IF @BackupType IN('LOG') 
        AND EXISTS(
            SELECT 1 
            FROM
                sys.database_recovery_status AS drs 
            WHERE 
                drs.database_id = DB_ID(@DatabaseName) 
            AND drs.last_log_backup_lsn IS NULL
            )
		BEGIN;
			PRINT '*** WARNING ***: Database [' + @DatabaseName + '] has not had a FULL database backup. Log or differential backups cannot be taken.  Skipping'
        END;
        ELSE
        BEGIN;

		    /* Ensure folder exists */
		    IF @SeparateDatabaseFolders = 1
		    BEGIN;
			    SELECT @BackupDatabaseFolder = @BackupPath + @FolderSeparator + @DatabaseName;

			    IF @FolderSeparator = '\'	/* Only need to create subdirectories on Windows it seems */
			    BEGIN;
				    IF @WhatIf = 1
				    BEGIN;
					    PRINT '[WhatIf] Create directory: ' + @BackupDatabaseFolder
				    END;
				    ELSE
				    BEGIN;
					    EXEC xp_create_subdir @BackupDatabaseFolder;
				    END;
			    END;
		    END;

		    /* Do the backup */
		    SELECT @StartedAt = GETDATE();
		    PRINT 'Backing up: ' + @DatabaseName
		    PRINT '';
		    SET @BackupCommandStubFileList = '';

		    SELECT @BackupCommandStubFileList = @BackupCommandStubFileList +'    ,DISK = ''' + REPLACE(REPLACE(@FullBackupPath, '##FILENUMBER##', CONVERT(NVARCHAR(10), n.Num)), '##TIMESTAMP##',  + CONVERT(NVARCHAR(30), GETUTCDATE(), 112) + '_' + REPLACE(CONVERT(NVARCHAR(10), GETUTCDATE(), 108), ':', '')) + ''''
		    FROM
			    @NumbersTable AS n
		    WHERE
			    n.Num < @NumberOfFiles
		    ;

		    SELECT @BackupCommandStubFileList = STUFF(@BackupCommandStubFileList, 1, 5, '    ')

		    SELECT @BackupCommand = REPLACE(@BackupCommandStub, '##DATABASE##', QUOTENAME(@DatabaseName)) + REPLACE(@BackupCommandStubFileList, '##DATABASE##', @DatabaseName)
		    SELECT @BackupCommand = @BackupCommand + @CRLF + @BackupWith

		    IF @WhatIf = 1
		    BEGIN;
			    PRINT '[WhatIf] Backup: ' + @BackupCommand;
		    END;
		    ELSE
		    BEGIN;
			    EXEC [master].dbo.sp_executesql @BackupCommand;
		    END;

		    PRINT '';

		    /* Record timings */
		    INSERT INTO @BackupTimings(DatabaseName, BackupType, StartedAt, FinishedAt)
		    SELECT
			    @DatabaseName
			    ,@BackupType
			    ,@StartedAt
			    ,GETDATE()

        END;

		FETCH NEXT FROM cDatabase INTO @DatabaseName;
	END;

	CLOSE cDatabase;
	DEALLOCATE cDatabase;

	/* Output the timings */
	IF @WhatIf = 0
	BEGIN;
		SELECT
			*
		FROM
			@BackupTimings
		ORDER BY
			StartedAt
		;
	END;

    /* Tidy up */
    DROP TABLE #DatabasesToBackup;
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_backup_databases;
GO