PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_updatestats2';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_updatestats2')
	EXEC ('CREATE PROC dbo.sp_updatestats2 AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE sp_updatestats2
	@Databases					NVARCHAR(MAX)	= '%'		/* Include all */
	,@Delimiter					NVARCHAR(10)	= ','
	,@IgnoreDatabases			NVARCHAR(MAX)	= NULL
	,@Resample					BIT = 0
AS
/*
Description: Procedure to loop over a list of databases and call sp_updatestats against them
Parameters:
  - Databases:          List of databases and/or wildcard search
  - Delimiter:          The delimiting character in the Databases list.  Defaults to ,
  - IgnoreDatabases:    List of databases to ignore
  - Resample:           Flag to determine if sp_updatestats should resample or not
*/
BEGIN;
    SET NOCOUNT ON;

    CREATE TABLE #Databases(
            DatabaseName	NVARCHAR(128) PRIMARY KEY CLUSTERED
    );

    DECLARE @DatabaseListXML	XML;
    SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@Databases, @Delimiter, '</db><db>') + '</db>' AS XML);

    WITH DatabaseList(DatabaseName) AS (
        SELECT DISTINCT
            LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
        FROM
            @DatabaseListXML.nodes('db') AS DB(N)
    )
    INSERT INTO #Databases(DatabaseName)
    SELECT
        LTRIM(RTRIM(d.[name]))	AS DatabaseName		/* Strip off leading and trailing spaces from database names (because they cause problems) */
    FROM
        sys.databases AS d
    WHERE
        EXISTS(
            SELECT 1 FROM DatabaseList WHERE d.name LIKE DatabaseName
        )
    AND	d.database_id != 2				/* Ignore tempdb */
    AND d.source_database_id IS NULL	/* Ignore database snapshots */
    AND d.[state] = 0					/* Online databases only */
    AND d.is_in_standby = 0				/* Not in standby mode */
    ;

    /* Remove any databases that should be ignored */
    SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@IgnoreDatabases, @Delimiter, '</db><db>') + '</db>' AS XML);

    WITH IgnoreDatabaseList(DatabaseName) AS (
        SELECT DISTINCT
            LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
        FROM
            @DatabaseListXML.nodes('db') AS DB(N)
    )
    DELETE dtp
    FROM #Databases AS dtp
    WHERE 
        EXISTS(SELECT 1 FROM IgnoreDatabaseList AS idl WHERE dtp.DatabaseName LIKE idl.DatabaseName);

    /* Delete databases that are secondary AG replicas */
    DECLARE @SQL    NVARCHAR(MAX);
    SELECT @SQL = N'
        DELETE FROM b
        FROM
            #Databases AS b
            INNER JOIN sys.databases AS d
                ON b.DatabaseName = d.name
            INNER JOIN sys.dm_hadr_database_replica_states AS drs 
                ON drs.database_id = d.database_id
            INNER JOIN sys.dm_hadr_availability_replica_states AS ars 
                ON drs.replica_id = ars.replica_id
        WHERE 
	        ars.role_desc != ''PRIMARY''
        AND ars.is_local = 1
    ';

    EXEC sp_executesql @SQL;

    DECLARE @DatabaseName   NVARCHAR(128);
    DECLARE cDatabase       CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT
        DatabaseName
    FROM
        #Databases
    ORDER BY
        DatabaseName	/* Ensure the order is known and consistent between executions */
    ;

    OPEN cDatabase;
    FETCH NEXT FROM cDatabase INTO @DatabaseName;

    WHILE @@FETCH_STATUS = 0
    BEGIN;
        PRINT 'Updating statistics on: ' + @DatabaseName;
        SELECT @SQL = N'USE ' + QUOTENAME(@DatabaseName) + '
    EXEC sp_updatestats' + CASE WHEN @Resample = 1 THEN ' @resample = ''resample''' ELSE '' END + ';
        ';

        EXEC sp_executesql @SQL;
        --PRINT @SQL;
        FETCH NEXT FROM cDatabase INTO @DatabaseName;
    END;

    CLOSE cDatabase;
    DEALLOCATE cDatabase;

    IF OBJECT_ID('tempdb..#Databases') IS NOT NULL DROP TABLE #Databases;
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_updatestats2;
GO
