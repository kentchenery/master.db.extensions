﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_check_databases';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_check_databases')
	EXEC ('CREATE PROC dbo.sp_check_databases AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE [dbo].[sp_check_databases]
	@Databases					NVARCHAR(MAX)	= '%'		/* Include all */
	,@Delimiter					NVARCHAR(10)	= ','
	,@IgnoreDatabases			NVARCHAR(MAX)	= NULL		/* Ignore none */
	,@NoIndex					BIT				= 0
	,@AllErrorMessages			BIT				= 0
	,@ExtendedLogicalChecks		BIT				= 0
	,@NoInfoMessages			BIT				= 0
	,@TabLock					BIT				= 0
	,@EstimateOnly				BIT				= 0
	,@PhysicalOnly				BIT				= 0
	,@DataPurity				BIT				= 0
	,@MaxDOP					TINYINT			= NULL
	,@IgnoreReadOnly			BIT				= 0
AS
BEGIN;

	SET NOCOUNT ON;

	DECLARE @CRLF NCHAR(2);
	SET @CRLF = CHAR(13) + CHAR(10);

	IF @DataPurity = 1 AND @PhysicalOnly = 1
	BEGIN;
		RAISERROR('Can only specify one of @PhysicalOnly and @DataPurity', 16, 1);
	END;

	/* Generate the list of databases to backup.  Use the FOR XML trick/hack to pivot the @Databases list */
	DECLARE @DatabasesList TABLE (
		DatabaseName	NVARCHAR(128) PRIMARY KEY CLUSTERED
	);

	DECLARE @DatabaseListXML	XML;
	SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@Databases, @Delimiter, '</db><db>') + '</db>' AS XML);

	WITH cDatabaseList(DatabaseName) AS (
		SELECT DISTINCT
			LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
		FROM
			@DatabaseListXML.nodes('db') AS DB(N)
	)
	INSERT INTO @DatabasesList(DatabaseName)
	SELECT
		d.[name]	AS DatabaseName
	FROM
		sys.databases AS d
	WHERE EXISTS(
			SELECT 1 FROM cDatabaseList WHERE d.name LIKE DatabaseName
		)
	AND	d.database_id != 2	/* Ignore tempdb */
	AND (
		
			@IgnoreReadOnly = 1
		OR	d.is_read_only = 0
	)

	SELECT @DatabaseListXML = CAST('<db>' + REPLACE(@IgnoreDatabases, @Delimiter, '</db><db>') + '</db>' AS XML);

	WITH IgnoreDatabaseList(DatabaseName) AS (
		SELECT DISTINCT
			LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS DatabaseName
		FROM
			@DatabaseListXML.nodes('db') AS DB(N)
	)
	DELETE dtp
	FROM @DatabasesList AS dtp
	WHERE 
		EXISTS(SELECT 1 FROM IgnoreDatabaseList AS idl WHERE dtp.DatabaseName LIKE idl.DatabaseName);

	/* Loop over the required databases and do the DBCC */
	DECLARE DatabaseCursor CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT DatabaseName FROM @DatabasesList;

	DECLARE @DatabaseName	NVARCHAR(128);
	DECLARE @Command		NVARCHAR(MAX);
	DECLARE @With			NVARCHAR(MAX);

	OPEN DatabaseCursor;
	FETCH NEXT FROM DatabaseCursor INTO @DatabaseName;

	WHILE @@FETCH_STATUS = 0
	BEGIN;
		
		/* Build the DBCC command */
		SELECT @Command = 'DBCC CHECKDB(' + QUOTENAME(@DatabaseName) + 
			CASE WHEN @NoIndex = 1 THEN ', NOINDEX' ELSE '' END + ')'

		SET @With = ''
			+ CASE WHEN @AllErrorMessages = 1		THEN '    ,ALL_ERRORMSGS' + @CRLF ELSE '' END
			+ CASE WHEN @MaxDOP IS NOT NULL			THEN '    ,MAXDOP = ' + CONVERT(VARCHAR(10), @MaxDOP)  + @CRLF ELSE '' END
			+ CASE WHEN @ExtendedLogicalChecks = 1	THEN '    ,EXTENDED_LOGICAL_CHECKS' + @CRLF ELSE '' END
			+ CASE WHEN @NoInfoMessages = 1			THEN '    ,NO_INFOMSGS' + @CRLF ELSE '' END
			+ CASE WHEN @TabLock = 1				THEN '    ,TABLOCK' + @CRLF ELSE '' END
			+ CASE WHEN @EstimateOnly = 1			THEN '    ,ESTIMATE_ONLY' + @CRLF ELSE '' END
			+ CASE WHEN @PhysicalOnly = 1			THEN '    ,PHYSICAL_ONLY' + @CRLF ELSE '' END
			+ CASE WHEN @DataPurity = 1				THEN '    ,DATA_PURITY' + @CRLF ELSE '' END
		;

		IF LEN(@With) > 4
		BEGIN;
			SELECT @With = @CRLF + STUFF(@With, 1, 5, 'WITH' + @CRLF + '    ')
		END;

		SELECT @Command = @Command + @With;
	
		PRINT '';
		PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' Checking database: ' + @DatabaseName;

		EXEC [master].dbo.sp_executesql @Command;

		PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' Check complete';
		PRINT '';
	
		FETCH NEXT FROM DatabaseCursor INTO @DatabaseName;
	END;
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_check_databases;
GO