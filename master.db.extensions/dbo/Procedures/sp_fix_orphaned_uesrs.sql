﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_fix_orphaned_users';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_fix_orphaned_users')
	EXEC ('CREATE PROC dbo.sp_fix_orphaned_users AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE sp_fix_orphaned_users
    @ReportOnly BIT = 0x0
AS
/*
---
Description:    Looks for users who are orphaned and tries to match them to logins with the same name but
                do not have a mapped user

Parameters:
 - ReportOnly: Flag to drive whether it simply reports which users are orphaned and possible matches

Output:
 - Will print out the users it has mapped
...
*/
BEGIN;
    DECLARE @OrphanedUsers TABLE(
        UserName                NVARCHAR(128)
        ,HasMatchingLogin       BIT DEFAULT(0)
        ,LoginAlreadyMapped     BIT DEFAULT(0)
    );

    INSERT INTO @OrphanedUsers(UserName, HasMatchingLogin, LoginAlreadyMapped)
    SELECT
	    dp.name
        ,CASE WHEN potentials.name IS NOT NULL THEN 1 ELSE 0 END	AS HasMatchingName
	    ,CASE WHEN dp2.sid IS NULL THEN 0 ELSE 1 END				AS LoginAlreadyMapped
    FROM 
	    sys.database_principals AS dp  
	    LEFT JOIN sys.server_principals AS sp  
		    ON dp.sid = sp.sid
	    LEFT OUTER JOIN sys.server_principals AS potentials
		    ON dp.name = potentials.name
	    LEFT OUTER JOIN sys.database_principals AS dp2
		    ON potentials.sid = dp2.sid
    WHERE
	    sp.sid IS NULL  
    AND dp.authentication_type_desc = 'INSTANCE'
    ;

    IF @ReportOnly = 0x1
    BEGIN;
        SELECT * FROM @OrphanedUsers;
        RETURN;
    END;

    /* Fix the logins */
    DECLARE @UserName NVARCHAR(128);

    DECLARE AdoptOrphans CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT UserName
    FROM
        @OrphanedUsers
    WHERE
        HasMatchingLogin = 1
    AND LoginAlreadyMapped = 0;

    OPEN AdoptOrphans;
    FETCH NEXT FROM AdoptOrphans INTO @UserName;

    DECLARE @SQL NVARCHAR(MAX);

    WHILE @@FETCH_STATUS = 0
    BEGIN;
        SELECT @SQL = REPLACE('ALTER USER {UserName} WITH LOGIN = {UserName}', '{UserName}', QUOTENAME(@UserName));

        EXEC sp_executesql @SQL;

        FETCH NEXT FROM AdoptOrphans INTO @UserName;
    END;

    CLOSE AdoptOrphans;
    DEALLOCATE AdoptOrphans;
;
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_fix_orphaned_users;
GO
