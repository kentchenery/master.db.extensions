﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_reindex';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_reindex')
	EXEC ('CREATE PROC dbo.sp_reindex AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE sp_reindex
     @DatabaseIncludeList			NVARCHAR(MAX) = NULL
    ,@DatabaseExcludeList			NVARCHAR(MAX) = NULL
    ,@ReorganizeThresholdPct		INT = 10
    ,@RebuildThresholdPct			INT = 30
    ,@MaxRebuildSizeMB				INT = 2048	    /* Maximum size of the table we will rebuild. Will be changed to reorganise if above this value */
    ,@MinRebuildSizeMB				INT = 2 	    /* Anything smaller than this will be completely ignored */
    ,@MaxPartitionSizeMB			INT = 5192	    /* Any partitions bigger than this will be skipped entirely */
    ,@UpdateStatisticsOnReorganize	BIT = 0         /* Updates the index statistics if a reorg is called against it */
    ,@SchemaIncludeList				NVARCHAR(MAX) = NULL
    ,@SchemaExcludeList				NVARCHAR(MAX) = NULL
    ,@ObjectIncludeList				NVARCHAR(MAX) = NULL
    ,@ObjectExcludeList				NVARCHAR(MAX) = NULL
    ,@WhatIf						BIT = 0
    ,@Debug							BIT = 1
AS
BEGIN;

    /* Options */
    SET NOCOUNT ON;

    /* Check input variables that might clash */
    IF @SchemaExcludeList IS NOT NULL AND @SchemaIncludeList IS NOT NULL
    BEGIN;
        RAISERROR('Can only specify one of @SchemaIncludeList and @SchemaExcludeList', 16, 1);
    END;

    IF @ObjectExcludeList IS NOT NULL AND @ObjectIncludeList IS NOT NULL
    BEGIN;
	    RAISERROR('Can only specify one of @SchemaIncludeList and @SchemaExcludeList', 16, 1);
    END;


    DECLARE @SQL NVARCHAR(MAX);	/* Used for dynamic SQL */

    /* Check input */
    IF (@ReorganizeThresholdPct > @RebuildThresholdPct)
    BEGIN;
	    RAISERROR('Reorganize threshold cannot be higher than rebuild threshold. If you do not want reorganize to run set it to NULL', 16, 1);
	    RETURN;
    END;

    /* Work out which databases this should apply to */
    DECLARE @Databases TABLE(
	    DatabaseId		INT
	    ,[Name]			NVARCHAR(128)
    )

    INSERT INTO @Databases
    SELECT
	    d.database_id, d.name
    FROM
	    sys.databases AS d
    WHERE 
	    (
	    /* Use the include list to limit what we are looking at.  If @DatabaseIncludeList is null then include all */
	    EXISTS(
		    SELECT 1 
		    FROM
		    (
			    SELECT
				    List.DatabaseName.value('.', 'NVARCHAR(128)') AS DatabaseName
			    FROM
				    (
					    SELECT CONVERT(XML, '<dbList><db>' + REPLACE(@DatabaseIncludeList, ',','</db><db>') + '</db></dbList>') AS DatabaseXml
				    ) AS DatabaseXmlList
				    CROSS APPLY	DatabaseXml.nodes('dbList/db') AS List(DatabaseName)
		    ) AS db
		    WHERE
			    db.DatabaseName = d.name
		    )
	    OR	@DatabaseIncludeList IS NULL
	    )
    AND NOT EXISTS(
	    /* And exclude any in the exclude list */
	    SELECT 1 
	    FROM
	    (
		    SELECT
			    List.DatabaseName.value('.', 'NVARCHAR(128)') AS DatabaseName
		    FROM
			    (
				    SELECT CONVERT(XML, '<dbList><db>' + REPLACE(@DatabaseExcludeList, ',','</db><db>') + '</db></dbList>') AS DatabaseXml
			    ) AS DatabaseXmlList
			    CROSS APPLY	DatabaseXml.nodes('dbList/db') AS List(DatabaseName)
	    ) AS db
	    WHERE
		    db.DatabaseName = d.name
    )
    AND d.name NOT IN ('tempdb');	/* Never do tempdb */

    /* Determine which indexes need maintenance */
    CREATE TABLE #IndexList(
	    DatabaseId			INT
	    ,ObjectId			INT
	    ,IndexId			INT
	    ,[Partition]		INT
	    ,[PageCount]		INT
	    ,DatabaseName		NVARCHAR(128)
	    ,SchemaName			NVARCHAR(128)
	    ,ObjectName			NVARCHAR(128)
	    ,IndexName			NVARCHAR(128)
	    ,Fragmentation		FLOAT
	    ,IsRebuild			BIT
	    ,IsReorganisation	BIT
	    ,IsPartitioned		BIT
	    ,IsTooSmall			BIT
	    ,IsTooLarge			BIT
	    ,IsRebuildTooLarge	BIT
    );

    DECLARE @DatabaseName   NVARCHAR(128);
    DECLARE @DatabaseId     INT;
    DECLARE IndexFragmentationFinder CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT
        [Name]
        ,DatabaseId
    FROM
        @Databases
    ;

    OPEN IndexFragmentationFinder;
    FETCH NEXT FROM IndexFragmentationFinder INTO @DatabaseName, @DatabaseId;

    WHILE @@FETCH_STATUS = 0
    BEGIN;

		BEGIN TRANSACTION FragFinder;

        WITH Fragmentation AS (
	        SELECT
		        @DatabaseName																																AS DatabaseName
		        ,CASE WHEN f.page_count > (@MaxPartitionSizeMB * 1024 / 8) THEN 1 ELSE 0 END																AS IsTooLarge
		        ,CASE WHEN f.page_count < (@MinRebuildSizeMB * 1024 / 8) THEN 1 ELSE 0 END																	AS IsTooSmall
		        ,CASE WHEN f.avg_fragmentation_in_percent >= @RebuildThresholdPct 
			        AND  f.page_count > (@MaxRebuildSizeMB * 1024 / 8) THEN 1 ELSE 0 END																	AS IsRebuildTooLarge
		        ,CASE WHEN f.avg_fragmentation_in_percent >= @RebuildThresholdPct AND  f.page_count <= (@MaxRebuildSizeMB * 1024 / 8) THEN 1 ELSE 0 END		AS IsRebuild
		        ,CASE WHEN f.avg_fragmentation_in_percent >= @ReorganizeThresholdPct AND f.page_count > (@MaxRebuildSizeMB * 1024 / 8) THEN 1 ELSE 0 END	AS IsReorganise
		        ,f.*
	        FROM
		        sys.dm_db_index_physical_stats(@DatabaseId, null, null, null, null) AS f
	        WHERE
		        f.index_id BETWEEN 1 AND 255	/* Only interested in data indexes */
        )
        INSERT INTO #IndexList(DatabaseId, ObjectId, IndexId, [Partition], [PageCount], DatabaseName, SchemaName, ObjectName, IndexName, Fragmentation, IsRebuild, IsReorganisation, IsTooSmall, IsTooLarge, IsRebuildTooLarge)
        SELECT
	        database_id
	        ,object_id
	        ,index_id
	        ,partition_number
	        ,page_count
	        ,DB_NAME(database_id)
	        ,OBJECT_SCHEMA_NAME(object_id, database_id)
	        ,OBJECT_NAME(object_id, database_id)
	        ,NULL
	        ,avg_fragmentation_in_percent
	        ,IsRebuild
	        ,IsReorganise
	        ,IsTooSmall
	        ,IsTooLarge
	        ,IsRebuildTooLarge
        FROM
	        Fragmentation
        WHERE
	        1 = 1
        /* Only the indexes that need attention */
        AND (IsRebuild = 1 OR IsReorganise = 1)
        AND IsTooSmall = 0
        AND IsTooLarge = 0
        ;

		COMMIT TRANSACTION FragFinder;

        FETCH NEXT FROM IndexFragmentationFinder INTO @DatabaseName, @DatabaseId;
    END;

    CLOSE IndexFragmentationFinder;
    DEALLOCATE IndexFragmentationFinder;

    /* Remove objects based on the @SchemaIncludeList/@SchemaExcludeList/@ObjectIncludeList/@ObjectExcludeList */
    DELETE FROM #IndexList
    WHERE
	    (
		    @SchemaIncludeList IS NOT NULL
	    AND NOT EXISTS (
		    SELECT 1
		    FROM
			    (
				    SELECT
					    List.[Name].value('.', 'NVARCHAR(128)') AS [Name]
				    FROM
					    (
						    SELECT CONVERT(XML, '<nameList><name>' + REPLACE(@SchemaIncludeList, ',','</name><name>') + '</name></nameList>') AS NameListXml
					    ) AS NameListXml
					    CROSS APPLY	NameListXml.nodes('nameList/name') AS List([Name])
			    ) AS NameList
		    WHERE
			    SchemaName LIKE [Name]
		    )
	    )
    OR (
			    @SchemaExcludeList IS NOT NULL
		    AND EXISTS (
			    SELECT 1
			    FROM
				    (
					    SELECT
						    List.[Name].value('.', 'NVARCHAR(128)') AS [Name]
					    FROM
						    (
							    SELECT CONVERT(XML, '<nameList><name>' + REPLACE(@SchemaExcludeList, ',','</name><name>') + '</name></nameList>') AS NameListXml
						    ) AS NameListXml
						    CROSS APPLY	NameListXml.nodes('nameList/name') AS List([Name])
				    ) AS NameList
			    WHERE
				    SchemaName LIKE [Name]
		    )
	    )
    ;

    DELETE FROM #IndexList
    WHERE
	    (
		    @ObjectIncludeList IS NOT NULL
		    AND NOT EXISTS (
			    SELECT 1
			    FROM
				    (
					    SELECT
						    List.[Name].value('.', 'NVARCHAR(128)') AS [Name]
					    FROM
						    (
							    SELECT CONVERT(XML, '<nameList><name>' + REPLACE(@ObjectIncludeList, ',','</name><name>') + '</name></nameList>') AS NameListXml
						    ) AS NameListXml
						    CROSS APPLY	NameListXml.nodes('nameList/name') AS List([Name])
				    ) AS NameList
			    WHERE
				    ObjectName LIKE [Name]
		    )
	    )
	    OR (
			    @ObjectExcludeList IS NOT NULL
		    AND	EXISTS (
			    SELECT 1
			    FROM
				    (
					    SELECT
						    List.[Name].value('.', 'NVARCHAR(128)') AS [Name]
					    FROM
						    (
							    SELECT CONVERT(XML, '<nameList><name>' + REPLACE(@ObjectExcludeList, ',','</name><name>') + '</name></nameList>') AS NameListXml
						    ) AS NameListXml
						    CROSS APPLY	NameListXml.nodes('nameList/name') AS List([Name])
				    ) AS NameList
			    WHERE
				    ObjectName LIKE [Name]
		    )
	    )
    ;

    /* Set the index name (TODO: Check if the index is partitioned) */
    DECLARE @Database NVARCHAR(128)
    DECLARE cDatabase CURSOR FAST_FORWARD FOR
    SELECT DISTINCT DatabaseName FROM #IndexList;

    OPEN cDatabase;
    FETCH NEXT FROM cDatabase INTO @Database

    WHILE @@FETCH_STATUS = 0
    BEGIN;
	    SELECT @SQL = 'UPDATE il SET IndexName = i.name FROM ' + QUOTENAME(@Database) + '.sys.indexes AS i INNER JOIN #IndexList AS il ON i.object_id = il.ObjectId AND i.index_id = il.IndexId WHERE il.DatabaseName = ''' + @Database + ''';'
	    --PRINT @SQL;
	    EXEC sp_executeSQL @SQL;
	
	    FETCH NEXT FROM cDatabase INTO @Database
    END;

    CLOSE cDatabase;
    DEALLOCATE cDatabase;

    IF @Debug = 1
    BEGIN;
	    SELECT * FROM #IndexList;
    END;


    /* Prepare the reindexing statement */
    DECLARE @Schema			NVARCHAR(128);
    DECLARE @Object			NVARCHAR(128);
    DECLARE @Index			NVARCHAR(128);
    DECLARE @IsRebuild		BIT;
    DECLARE @IsReorganise	BIT;
    DECLARE @IsPartitioned	BIT;
    DECLARE @Partition		INT;

    DECLARE Reindexer CURSOR LOCAL FAST_FORWARD
    FOR 
    SELECT
	    DatabaseName
	    ,SchemaName
	    ,ObjectName
	    ,IndexName
	    ,IsRebuild
	    ,IsReorganisation
	    ,ISNULL(IsPartitioned, 0)
	    ,[Partition]
    FROM
	    #IndexList
    ;

    OPEN Reindexer;
    FETCH NEXT FROM Reindexer INTO @Database, @Schema, @Object, @Index, @IsRebuild, @IsReorganise, @IsPartitioned, @Partition;

    WHILE @@FETCH_STATUS = 0
    BEGIN;
	    SELECT
		    @SQL = 'USE ' + QUOTENAME(@Database) + CHAR(13) + CHAR(10) 
			    --+ 'GO' + CHAR(13) + CHAR(10)
			    + 'ALTER INDEX ' + QUOTENAME(@Index) + ' ON ' + QUOTENAME(@Schema) + '.' + QUOTENAME(@Object) + 
			    + CASE WHEN @IsRebuild = 1 THEN ' REBUILD' ELSE ' REORGANIZE' END
			    + CASE WHEN @IsPartitioned = 1 THEN ' PARTITION = ' + LTRIM(STR(@Partition)) ELSE '' END
			    ;

	    IF @WhatIf = 1
	    BEGIN;
		    PRINT @SQL;
	    END;
	    ELSE
	    BEGIN TRY;
		    BEGIN TRAN Reindexer;
			    PRINT CONVERT(NVARCHAR(30), GETDATE(), 121) + ' - ' + QUOTENAME(@Database) + '.' + QUOTENAME(@Schema) + '.' + QUOTENAME(@Object) + '.' + QUOTENAME(@Index) + '. Rebuild: ' + CONVERT(NVARCHAR(1), @IsRebuild) + ' Reorg: ' + CONVERT(NVARCHAR(1), @IsReorganise) + ' Partitioned: ' + CONVERT(NVARCHAR(1), @IsPartitioned)
	
			    DECLARE @StartDate	DATETIME;
			    DECLARE @EndDate	DATETIME;
			    SELECT @StartDate = GETDATE();

			    EXEC sys.sp_executesql @SQL;

                IF @IsReorganise = 1 AND @UpdateStatisticsOnReorganize = 1
                BEGIN;
                    SELECT @SQL = 'UPDATE STATISTICS ' + QUOTENAME(@Schema) + '.' + QUOTENAME(@Object) + '(' + QUOTENAME(@Index) + ')'
                        + CASE WHEN @IsPartitioned = 1 THEN ' ON PARTITION (' + LTRIM(STR(@Partition)) + ')' ELSE '' END
                END;

			    SELECT @EndDate = GETDATE();

			    PRINT CONVERT(NVARCHAR(30), GETDATE(), 121) + ' - Duration: ' + LTRIM(STR(DATEDIFF(SECOND, @StartDate, @EndDate)));
		    COMMIT TRAN Reindexer;
	    END TRY
	    BEGIN CATCH;
		    ROLLBACK TRANSACTION Reindexer;

            DECLARE @ErrorMessage NVARCHAR(100);
            SELECT @ErrorMessage =  N'Failed to reindex: ' + QUOTENAME(@Schema) + '.' + QUOTENAME(@Object) + '(' + QUOTENAME(@Index) + ')';
		    
            RAISERROR(@ErrorMessage, 16, 1);
	    END CATCH;
	    FETCH NEXT FROM Reindexer INTO @Database, @Schema, @Object, @Index, @IsRebuild, @IsReorganise, @IsPartitioned, @Partition;

    END;

    /* Drop the temp table */
    DROP TABLE #IndexList;
END;
GO


/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_reindex;
GO