﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_delete_old_files';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_delete_old_files')
	EXEC ('CREATE PROC dbo.sp_delete_old_files AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE [dbo].[sp_delete_old_files]
	@Path					    NVARCHAR(1000)	= NULL,
	@IncludeSubdirectories		INT				= 1,
	@DeleteOlderThan			DATETIME		= NULL,
	@Extensions					NVARCHAR(1000)	= NULL,
	@Delimiter					NVARCHAR(5)		= ','
AS
/*
---
Description:    Extends the xp_delete_file extended procedure.  This procedure takes a path
				and an optional list of extensions.  It then loops over the extensions
				and calls the xp_delete_file procedure against them.  This has the effect of
				removing all files with the extensions supplied that were taken before the
				@DeleteOlderThan parameter.

Parameters:
 - Path:    				Folder that files are deleted from
 - IncludeSubdirectories:	Flag to indicate if subdirectories should be traversed
 - DeleteOlderThan:			Oldest date to retain files from. If the file is before this date it is deleted
 - Extensions:				Delimited list of file extensions.  If NULL is supplied it defaults to BAK,TRN,DIF,DIFF,LOG,TXT
 - Delimiter:				The deliminter used in @Extensions

Output:
 - Prints an informational message about the path its deleting in.
...
*/
BEGIN;

	/* Thanks to: http://stackoverflow.com/questions/24582996/sql-server-xp-delete-file-parameters */

	/* Turn off row counting */
	SET NOCOUNT ON;

	/* Look up a default path if none supplied - we use the backup path */
	IF @Path IS NULL
	BEGIN;
		EXEC master.dbo.xp_instance_regread
			N'HKEY_LOCAL_MACHINE'
			,N'Software\Microsoft\MSSQLServer\MSSQLServer'
			,N'BackupDirectory'
			,@Path OUTPUT
			,'no_output';
	END;

	/* Determine the folder separator */
	DECLARE @FolderSeparator	CHAR(1);

	IF LEFT(@Path, 1) = '/'	/* Is it on Linux? */
	BEGIN;
		SET @FolderSeparator = '/';
	END;
	ELSE
	BEGIN;
		SET @FolderSeparator = '\';
	END;

	IF RIGHT(@Path, 1) != @FolderSeparator
	BEGIN;
		SELECT @Path = @Path + @FolderSeparator;
	END;

	/* For now the xp_delete_file extended procedure cannot deal with Linux paths.  We swap these to C:\ instead */
		SET @FolderSeparator = '\';
		IF LEFT(@Path, 1) = '/'
		BEGIN;
			SELECT @Path = 'C:' + @Path;
		END;

		SELECT @Path = REPLACE(@Path, '/', '\');
	/* When the extended procedures support Linux paths correctly, the block of code above needs to be removed */

	IF @DeleteOlderThan IS NULL
	BEGIN;
		SELECT @DeleteOlderThan = DATEADD(DAY, -7, GETDATE());
	END;

	SELECT @Extensions = ISNULL(@Extensions, 'BAK,TRN,DIF,DIFF');

	/* Generate the list of extensions.  Use the FOR XML trick/hack to pivot the @Extensions list */
	DECLARE @ExtensionsList TABLE (
		Extension	NVARCHAR(128) PRIMARY KEY CLUSTERED
	);

	DECLARE @ExtensionsXML	XML;
	SELECT @ExtensionsXML = CAST('<ext>' + REPLACE(@Extensions, @Delimiter, '</ext><ext>') + '</ext>' AS XML);

	WITH cExtensions AS (
		SELECT DISTINCT
			LTRIM(RTRIM(N.value('.', 'NVARCHAR(128)'))) AS Extension
		FROM
			@ExtensionsXML.nodes('ext') AS Extension(N)
	)
	INSERT INTO @ExtensionsList
	SELECT
		Extension
	FROM
		cExtensions

	DECLARE @Extension NVARCHAR(1000);		/* Extension we will be deleting */

	/* Loop over the list of extensions deleting them from the folder(s) */
	DECLARE ExtensionCursor CURSOR LOCAL FAST_FORWARD
	FOR
		SELECT Extension FROM @ExtensionsList;

	OPEN ExtensionCursor;
	FETCH NEXT FROM ExtensionCursor INTO @Extension;
	
	WHILE (@@FETCH_STATUS = 0)
	BEGIN;
		PRINT 'Deleting files form before "' + CONVERT(NVARCHAR(100), @DeleteOlderThan, 121) + '" with the extension "' + @Extension + '" from the path "' + @Path + '"';
		
		/* Delete the files */
		EXEC [master].dbo.xp_delete_file 0, @Path, @Extension, @DeleteOlderThan, @IncludeSubdirectories;

		FETCH NEXT FROM ExtensionCursor INTO @Extension;
	END;

	CLOSE ExtensionCursor;
	DEALLOCATE ExtensionCursor;

END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_delete_old_files;
GO
GO
