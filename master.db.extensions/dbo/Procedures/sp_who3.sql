﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_who3';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_who3')
	EXEC ('CREATE PROC dbo.sp_who3 AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE sp_who3
	@spid                       INT = NULL
    ,@IncludeSystemSessions     BIT = 0
    ,@IncludeSelf               BIT = 0
	,@Database					NVARCHAR(128) = NULL
AS
/*
---
Description:    Provides information on running processes much like sp_who and sp_who2

Parameters:
    - spid:     session_id (aka spid).

Output:
 - Similar information to sp_who/sp_who2.
...
*/
BEGIN;
	SET NOCOUNT ON;

	SELECT
		s.session_id
		,ISNULL(r.status, 'sleeping') AS status
		,s.login_name
		,ISNULL(r.blocking_session_id, 0) AS blocking_session_id
		,r.cpu_time
		,r.reads            as phsyical_reads
        ,r.logical_reads
		,r.writes
		,DATEDIFF(SECOND, r.start_time, GETDATE()) AS duration_sec
		,DB_NAME(r.database_id)	as [database_name]
		,r.command
		,r.wait_type
		,st.query
		,r.start_time
		,r.percent_complete
		,DATEADD(SECOND, r.estimated_completion_time / 1000, GETDATE()) AS estimated_completion_date
        ,ISNULL(blocker.blocked_count,0) AS blocking_count
        ,qp.query_plan
	FROM
		sys.dm_exec_sessions AS s
        INNER JOIN sys.sysprocesses AS sp	ON s.session_id = sp.spid
		LEFT OUTER JOIN sys.dm_exec_requests AS r ON s.session_id = r.session_id
		OUTER APPLY (
			SELECT 
				a.text AS query
			FROM
				sys.dm_exec_sql_text(r.sql_handle) AS a
			FOR XML PATH(''), TYPE
		) AS st(query)
        OUTER APPLY (
            SELECT COUNT(*) AS blocked_count
            FROM
                sys.dm_exec_requests AS blocked
            WHERE
                blocked.blocking_session_id = r.session_id
        ) AS blocker
        OUTER APPLY sys.dm_exec_query_plan(r.plan_handle) AS qp
	WHERE
		1 = 1
	AND (s.session_id = @spid OR (@spid IS NULL AND r.status != 'sleeping' AND r.status IS NOT NULL))
    AND (s.is_user_process = 1 OR @IncludeSystemSessions = 1)
    AND (s.session_id != @@SPID OR s.session_id = @spid OR @IncludeSelf = 1)
	AND (sp.dbid = DB_ID(@Database) OR @Database IS NULL)
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_who3;
GO