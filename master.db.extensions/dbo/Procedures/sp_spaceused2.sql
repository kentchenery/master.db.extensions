﻿PRINT CONVERT(NVARCHAR(30), GETUTCDATE(), 127) + ' | Deploying sp_spaceused2';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'sp_spaceused2')
	EXEC ('CREATE PROC dbo.sp_spaceused2 AS SELECT ''stub version, to be replaced''')
GO

ALTER PROCEDURE sp_spaceused2
	@ObjectName NVARCHAR(256)	= NULL
AS
/*
---
Description:    Returns sizing information about the object supplied.  Similar to sp_spaceused
				but with some extra columns.  Also data is returned in MB size and without text.

Parameters:
 - ObjectName:  Name of the object you want sizing information about

Output:
 - Similar information to sp_spaceused.
...
*/
BEGIN;

	DECLARE @ObjectID	INT;
	SELECT @ObjectID = OBJECT_ID(@ObjectName);

	IF @ObjectName IS NOT NULL AND @ObjectID IS NULL
	BEGIN;
		DECLARE @ErrorMessage NVARCHAR(MAX);
		SET @ErrorMessage = 'Unknown object "' + @ObjectName + '"';

		RAISERROR(@ErrorMessage, 16, 1);
	END;

	WITH TableSize AS (
		SELECT
			o.object_id														AS ObjectID
			,s.name															AS SchemaName
			,o.name															AS ObjectName
			,p.rows															AS [RowCount]
			,SUM(a.total_pages)												AS TotalPages
			,SUM(CASE WHEN i.index_id < 2 THEN a.data_pages END)			AS DataPages
			,SUM(CASE WHEN i.index_id > 1 THEN a.data_pages END)			AS IndexPages
			,SUM(a.used_pages - a.data_pages)								AS InternalOverheadPages
			,SUM(a.total_pages)- SUM(a.used_pages)							AS UnusedPages
		FROM
			sys.objects AS o
			INNER JOIN sys.schemas AS s				ON o.schema_id = s.schema_id
			INNER JOIN sys.indexes AS i				ON o.object_id = i.object_id
			INNER JOIN sys.partitions AS p			ON i.object_id = p.object_id AND i.index_id = p.index_id
			INNER JOIN sys.allocation_units AS a	ON p.partition_id = a.container_id
		WHERE
			(o.object_id = @ObjectID OR @ObjectID IS NULL)
		AND (@ObjectID IS NOT NULL OR o.[type] = 'U')
		GROUP BY
			o.object_id
			,s.name
			,o.name
			,p.rows
	)
	SELECT
		ObjectID
		,SchemaName
		,ObjectName
		,[RowCount]
		,CONVERT(DECIMAL(10, 2), TotalPages * 8 / 1024.0)				AS TotalSizeMB
		,CONVERT(DECIMAL(10, 2), DataPages * 8 / 1024.0)				AS DataSizeMB
		,CONVERT(DECIMAL(10, 2), ISNULL(IndexPages, 0) * 8 / 1024.0)	AS IndexSizeMB
		,CONVERT(DECIMAL(10, 2), InternalOverheadPages * 8 / 1024.0)	AS InternalOverheadSizeMB
		,CONVERT(DECIMAL(10, 2), UnusedPages * 8 / 1024.0)				AS UnusedSizeMB
		,TotalPages
		,DataPages
		,ISNULL(IndexPages, 0)											AS IndexPages
		,InternalOverheadPages
		,UnusedPages
	FROM
		TableSize AS ts
	ORDER BY
		SchemaName
		,ObjectName
END;
GO

/* Mark system objects */
EXEC sys.sp_MS_marksystemobject sp_spaceused2;
GO