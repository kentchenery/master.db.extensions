﻿PRINT CONVERT(NVARCHAR(30), CONVERT(DATETIME2(3), SYSUTCDATETIME()), 127) + ' | Deploying Uptime';
GO
USE [master]
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Uptime')
	EXEC ('CREATE VIEW dbo.Uptime AS SELECT ''stub version, to be replaced'' AS Stub')
GO

ALTER VIEW [dbo].[Uptime]
AS
WITH cteUptime AS (
	SELECT
		create_date															AS ServerStartDate
		,DATEDIFF(SECOND, create_date, sysdatetime())						AS UptimeTotalSeconds
	FROM
		sys.databases
	WHERE
		database_id = 2
), UptimeDays AS (

	SELECT
		ServerStartDate
		,UptimeTotalSeconds
		,UptimeTotalSeconds / (24 * 60 * 60)	AS UptimeDays 
		,UptimeTotalSeconds % (24 * 60 * 60)	AS Remainder
	FROM cteUptime

), UptimeHours AS (

	SELECT
		ServerStartDate
		,UptimeDays
		,Remainder / (60 * 60)	AS UptimeHours
		,Remainder % (60 * 60)	AS Remainder
	FROM
		UptimeDays

), UptimeMinutes AS (

	SELECT
		ServerStartDate
		,UptimeDays
		,UptimeHours
		,Remainder / 60			AS UptimeMinutes
		,Remainder % 60			AS Remainder
	FROM
		UptimeHours

), UptimeSeconds AS (
	SELECT
		ServerStartDate
		,UptimeDays
		,UptimeHours
		,UptimeMinutes
		,Remainder % 60 AS UptimeSeconds
	FROM
		UptimeMinutes
)
SELECT
	ServerStartDate
	,UptimeDays
	,UptimeHours
	,UptimeMinutes
    ,UptimeSeconds
	,CONVERT(NVARCHAR(10), UptimeDays) + 'd '
	+ RIGHT('0' + CONVERT(NVARCHAR(2), UptimeHours), 2) + ':' 
	+ RIGHT('0' + CONVERT(NVARCHAR(2), UptimeMinutes), 2) + ':'
	+ RIGHT('0' + CONVERT(NVARCHAR(2), UptimeSeconds), 2)				AS Uptime
FROM
	UptimeSeconds